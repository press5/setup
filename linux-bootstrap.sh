#!/bin/sh

# apt install curl git rbenv vim


[ -d ~/bin ] || mkdir ~/bin
git clone https://github.com/pyenv/pyenv.git ~/bin/pyenv
git clone https://github.com/pyenv/pyenv-virtualenv.git ~/bin/pyenv/plugins/pyenv-virtualenv

