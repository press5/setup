# hello, ship
export ZSH="$HOME/.oh-my-zsh"
ZSH_CUSTOM="$HOME/.omz"
ZSH_THEME="jkl"
CASE_SENSITIVE="false"
HYPHEN_INSENSITIVE="false"
zstyle ':omz:update' mode reminder   # disabled|auto|reminder
zstyle ':omz:update' frequency 28
DISABLE_MAGIC_FUNCTIONS="false"
DISABLE_LS_COLORS="false"
DISABLE_AUTO_TITLE="false"
ENABLE_CORRECTION="false"
COMPLETION_WAITING_DOTS="%F{yellow}working...%f"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="false" # fixed in theme
#setopt appendhistory
# HIST_STAMPS="yyyy-mm-dd"
HIST_STAMPS="%G-%m-%dT%H:%M:%SZ"
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
HISTSIZE=2000
SAVEHIST=1000
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git history fzf)

### conditional changes
# if sdf add bsd paths
if env | grep -q arpa; then
 export PATH="$PATH:/usr/pkg/bin:/usr/local/bin:/usr/bin:/bin:/usr/pkg/games:/usr/pkg/X11R7/bin"
fi

# set prompt if cathode
if env | grep -q Cathode; then
        ZSH_THEME="amuse"
fi

source $ZSH/oh-my-zsh.sh

### keychain
if [ `id -un` != root ]
 then
  # start up keychain to handle ssh-agent
  if [ -x /usr/bin/keychain ];
    then /usr/bin/keychain ~/.ssh/id_ed25519 ~/.ssh/id_ecdsa ~/.ssh/id_rsa;
  fi
  if [ -x /usr/local/bin/keychain ];
    then /usr/local/bin/keychain ~/.ssh/id_ed25519 ~/.ssh/id_ecdsa ~/.ssh/id_rsa;
  fi
  if [ -x /opt/homebrew/bin/keychain ];
    then /opt/homebrew/bin/keychain ~/.ssh/id_ed25519 ~/.ssh/id_ecdsa ~/.ssh/id_rsa;
  fi
   source ~/.keychain/`hostname`-sh   # load env vars
fi

### exports
export EDITOR='vim'
### aliases
alias swd='say -v bells pipeline is done'
alias ssh="/usr/bin/ssh -A"
alias mkpasswd="docker run rlesouef/alpine-makepasswd"
alias fabulous='yes "$(seq 231 -1 16)" | while read i; do printf "\x1b[48;5;${i}m\n"; sleep .02; done'
alias ds="dig +short"
alias dt="dig +trace"
alias yl="yamllint"
alias hist="history | grep -i $1"
alias udate="date -u +%G-%m-%dT%H:%M:%SZ"
alias wzir=exit

alias k="kubectl"
alias kcset="kubectx $1"
alias kcunset="kubectx -u"
alias kcshow="kubectx"
alias drc="docker-compose"

### PATHING

## perlbin stuff, if needed
#export PATH=${0:A:h}/perl5/bin:$PATH
#export PERL5LIB=/Users/jkl/perl5/lib/perl5
#export PERL_LOCAL_LIB_ROOT=/Users/jkl/perl5
#export PERL_MB_OPT='--install_base "/Users/jkl/perl5"'
#export PERL_MM_OPT=INSTALL_BASE=/Users/jkl/perl5

### jklbin
export PATH=${0:A:h}/bin:$PATH

### rbenv
eval "$(rbenv init - zsh)"

### python - must be last path or pyenv gets fussy
#export PYENV_VIRTUALENV_VERBOSE_ACTIVATE=1
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"  # must be the last PATH export
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

### functions

function certinfo() {
  cihost="$1"
  ciport="${2:-443}"
  echo | openssl s_client -showcerts -servername $cihost -connect $cihost:$ciport 2>/dev/null | openssl
x509 -inform pem -noout -text | head -11
}


# export LANG=en_US.UTF-8
# export ARCHFLAGS="-arch x86_64"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

