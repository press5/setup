#!/usr/bin/env sh
# quick setup
# good luck, future me


brew_fonts () {
  echo ">> installing fonts..."
  brew tap homebrew/cask-fonts
  brew install --cask font-monocraft font-jetbrains-mono-nerd-font \
    font-intel-one-mono font-ibm-plex-mono font-optician-sans
}


brew_casks () {
  echo ">> installing casks and taps..."
  brew install --cask arduino-ide bartender goland insomnia iterm2 itsycal \
    karabiner-elements keepingyouawake muzzle obsidian opera pycharm \
    qmk-toolbox vivaldi
}

brew_formulas () {
  echo ">> installing formulae..."
  brew install ack coreutils cpanminus freeipmi fzf hping htop httpie httpx \
    iftop ipcalc ipmitool jq jsonlint jsonnet jsonnet-bundler keychain \
    mtr ncdu pcre2 openssl prettyping pyenv pyenv-virtualenv pyyaml qemu \
    readline rust sshpass tmux tree um utf8proc watch wget wakeonlan xz \
    yamllint yq
}

conf_defaults () {
  echo ">> setting system defaults..."
  #lazybackup
  defaults read > ~/$USER.defaults.prev
  # some UI color
  defaults write NSGlobalDomain AppleHighlightColor -string "1.000000 0.874510 0.701961 Orange"
  defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"
  defaults write NSGlobalDomain AppleInterfaceStyleSwitchesAutomatically -bool true
  defaults write NSGlobalDomain AppleAccentColor -int 1
  defaults write NSGlobalDomain AppleAquaColorVariant -int 1
  defaults write NSGlobalDomain com.apple.springing.enabled -bool true
  defaults write NSGlobalDomain com.apple.springing.delay -float "0.5"
  # localization
  defaults write NSGlobalDomain AppleICUForce24HourTime -bool true
  defaults write NSGlobalDomain AppleMetricUnits -bool false
  defaults write NSGlobalDomain AppleMeasurementUnits -string "Inches"
  defaults write NSGlobalDomain AppleTemperatureUnit -string "Fahrenheit"
  # disable automatic substitution annoyances
  defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
  defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
  defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
  defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
  # finder prefs
  defaults write com.apple.finder NewWindowTarget -string "PfHm" # home directory
  defaults write com.apple.finder FXDefaultSearchScope -string "SCcf" # current dir
  defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv" # list; other modes: icnv, clmv, Flwv
  defaults write com.apple.finder FXPreferredGroupBy -string "Kind" 
  defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
  defaults write com.apple.finder WarnOnEmptyTrash -bool false
  defaults write com.apple.finder EmptyTrashSecurely -bool false  # default; here for reference
  defaults write com.apple.finder ShowPathbar -bool true
  defaults write com.apple.finder QLEnableTextSelection -bool true  # quick look
  defaults write com.apple.finder _FXShowPosixPathInTitle -bool false
  # dock
  defaults write com.apple.dock show-recents -bool false
  defaults write com.apple.dock minimize-to-application -bool true
  defaults write com.apple.dock enable-spring-load-actions-on-all-items -bool true
  defaults write com.apple.dock launchanim -bool true
  defaults write com.apple.dock expose-group-by-app -bool true
  defaults write com.apple.dock show-process-indicators -bool true
  defaults write com.apple.dock mineffect -string "scale"
  # hid preferences
  defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false
  defaults write NSGlobalDomain AppleMiniaturizeOnDoubleClick -bool false
  defaults write NSGlobalDomain AppleScrollerPagingBehavior -bool true
  defaults write NSGlobalDomain com.apple.trackpad.forceClick -bool true
  # a11y
  defaults write NSGlobalDomain com.apple.sound.beep.sound -string "/System/Library/Sounds/Basso.aiff"
  defaults write NSGlobalDomain com.apple.sound.beep.feedback -int 1
  defaults write NSGlobalDomain com.apple.sound.beep.flash -int 1
  defaults write com.apple.accessibility DifferentiateWithoutColor -bool true
  defaults write com.apple.accessibility ReduceMotionEnabled -bool true
  defaults write com.apple.siri StatusMenuVisible -bool false
  # misc
  defaults write com.apple.scriptmenu.plist ScriptMenuEnabled true
  defaults write com.apple.Safari IncludeInternalDebugMenu -bool true
  
}

check_xcode () {
  echo ">> checking for xcode..."
  xcode-select -p
  if [[ $? = 2 ]]; then
    xcode-select --install
  fi
}

inst_brew () {
  echo ">> checking for brew..."
  which -s brew
  if [[ $? != 0 ]]; then
    echo "installing homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)";
  else
    echo "updating brew..."
    brew update
  fi
}

# main subroutine

do_setup () {
  echo ">> beginning setup..."
  check_xcode
  inst_brew
  brew_fonts
  brew_casks
  brew_formulas
  conf_defaults
}

# main program
do_setup

# things i have yet to automate:
# - remap builtin keyboard caps to ctrl in sysprefs
# - install Moira


