"----- this is my vimrc, there are many like it but this one is mine -----

"----- neobundle stuff -----

if has('vim_starting')
  if &compatible
    set nocompatible
  endif
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif
call neobundle#begin(expand('~/.vim/bundle'))
NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'thiagoalessio/rainbow_levels.vim'
NeoBundle 'ciaranm/securemodelines'

NeoBundle 'vim-perl/vim-perl'
NeoBundle 'StanAngeloff/php.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'bling/vim-airline'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'vim-airline/vim-airline-themes'

NeoBundle 'christoomey/vim-run-interactive'
NeoBundle 'Valloric/vim-indent-guides'

NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }

call neobundle#end()

"----- shared code -----

filetype plugin indent on
NeoBundleCheck
"set t_ut=
set t_Co=256
set laststatus=2

" option name default optional " 20220225 jkl  what was this for?
let g:solarized_termcolors= 16
let g:solarized_termtrans = 1
let g:solarized_degrade = 0
let g:solarized_bold = 1
let g:solarized_underline = 1
let g:solarized_italic = 1
let g:solarized_contrast = "normal"
let g:solarized_visibility= "normal"
colorscheme solarized
set background=light
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
syntax on
filetype plugin indent on

"highlight Normal ctermbg=0

"let hostname = substitute(system('hostname'), '\n', '','')

if hostname() == "koloth"
 highlight Normal ctermbg=0
endif

set tabstop=2 shiftwidth=2 softtabstop=2 expandtab "sorry
set backspace=indent,eol,start
set nu

"disable vim's auto ++/-- so i don't accidentally screw things up using tmux
map <C-a> <Nop>
map <C-x> <Nop>

" remove trailing spaces at EOL
" autocmd FileType yaml autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\s\+$//e
"----- keybindings for plugins -----

nnoremap <leader>nt :NERDTreeToggle<CR>
nnoremap <leader>ri :RunInInteractiveShell<space>

"----- system specific -----

if hostname() == "koloth"
  highlight Normal ctermbg=0
endif

"----- modeline -----
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>
